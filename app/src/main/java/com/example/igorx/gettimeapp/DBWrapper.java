package com.example.igorx.gettimeapp;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBWrapper {
    private static final String DB_NAME = "db_data_time";
    private static final int DB_VERSION = 1;
    private static final String DB_TABLE = "data_db";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_DATA = "data";

    private static final String DB_CREATE =
            "create table " + DB_TABLE + "(" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_DATA + " text" +
                    ");";

    private Context mCtx;
    private DBHelper dbHelper;
    private SQLiteDatabase database;

    public DBWrapper(Context context){
        this.mCtx=context;
    }

   public void open(){
       dbHelper=new DBHelper(mCtx,DB_NAME,null,DB_VERSION);
       database=dbHelper.getWritableDatabase();
   }

    public void close(){
        if(dbHelper!=null){
            dbHelper.close();
        }
    }

    public Cursor getAllRecord(){
        if(database!=null){
            Log.i("data","");
          return database.query(DB_TABLE,null,null,null,null,null,null);
        }
        else {
            return  null;
        }
    }
    public void addRecord(String time) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_DATA, time);
        database.insert(DB_TABLE, null, cv);
    }

    public void clearRecord() {
       database.delete(DB_TABLE,null,null);
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

}
