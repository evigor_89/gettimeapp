package com.example.igorx.gettimeapp;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private Button btnChangeColor,btnLoadData,btnClearRec;
    private RelativeLayout relLayot;
    private TextView tvRecivedData;
    private ListView lvData;
    private SimpleCursorAdapter cursorAdapter;
    private DBWrapper dbWrapper;
    private Random rndColor;
    int backgroundColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        dbWrapper=new DBWrapper(this);
        dbWrapper.open();
        rndColor=new Random();
        customizeListData();
        getSupportLoaderManager().initLoader(0, null, this);
    }

    private void customizeListData() {
        String [] from={DBWrapper.COLUMN_DATA};
        int [] to ={R.id.tv_time};
        cursorAdapter=new SimpleCursorAdapter(this,R.layout.item_list_time,null,from,to,0);
        lvData.setAdapter(cursorAdapter);
    }

    private void initView() {
        btnChangeColor=(Button)findViewById(R.id.btn_color);
        btnLoadData=(Button)findViewById(R.id.btn_load_data);
        btnClearRec=(Button)findViewById(R.id.button_clear);
        relLayot=(RelativeLayout)findViewById(R.id.rel_layout);
        lvData=(ListView)findViewById(R.id.lv_data);
        tvRecivedData=(TextView)findViewById(R.id.tv_recived_data);

        btnChangeColor.setOnClickListener(this);
        btnLoadData.setOnClickListener(this);
        btnClearRec.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_color:
                changeBackgroundColor();
                break;
            case R.id.btn_load_data:
                responseData();
                Log.i("data","time current"+System.currentTimeMillis());
                break;
            case R.id.button_clear:
                clearData();
                break;

        }
    }

    private void clearData() {
        dbWrapper.clearRecord();
        getSupportLoaderManager().getLoader(0).forceLoad();
        tvRecivedData.setText(getResources().getString(R.string.received_data));
    }

    private void responseData() {
        new RequestTimeAsync().execute();
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String time = simpleDateFormat.format(date);
        tvRecivedData.setText(time);
        dbWrapper.addRecord(time);
        getSupportLoaderManager().getLoader(0).forceLoad();
    }

    private void changeBackgroundColor() {
      backgroundColor= Color.argb(140, rndColor.nextInt(256), rndColor.nextInt(256), rndColor.nextInt(256));
      relLayot.setBackgroundColor(backgroundColor);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbWrapper.close();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new TimeCursorLoader(this,dbWrapper);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
         cursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    static class TimeCursorLoader extends CursorLoader {
        DBWrapper db;
        public TimeCursorLoader(Context context,DBWrapper dbWrapper) {
            super(context);
            this.db=dbWrapper;
        }

        @Override
        public Cursor loadInBackground() {
            return db.getAllRecord();
        }
    }

    public class RequestTimeAsync extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... params) {
            Log.i("data","in doInBackground");
            String data=null;
            try {
               // data=getContent("http://android-logs.uran.in.ua/test.php");
                data=getHTTPString();
                Log.i("data","Loading DATA= "+data);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return data;
        }


    }

    private String getContent(String path) throws IOException {

        Log.i("data","in getContent");
        BufferedReader reader = null;
        try {
            URL url = new URL(path);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            c.setRequestMethod("GET");
            c.setReadTimeout(10000);
           //c.setRequestProperty("");
            c.setDoInput(true);
            c.setDoOutput(true);
            c.connect();


            reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
            Log.i("data","reader= "+reader);
            StringBuilder buf = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                buf.append(line + "\n");
            }
           // Log.i("data","buffer= "+buf.toString());
            return (buf.toString());
        }finally {
            if (reader!= null) {
                reader.close();
            }
        }
    }


/////////////////////////////////////

    private String getHTTPString() throws IOException {

        HttpClient httpClient= new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://android-logs.uran.in.ua/test.php");

        HttpResponse httpResponse = httpClient.execute(httpGet);
        InputStream inputStream =httpResponse.getEntity().getContent();
        String response =convertInputStreamToString(inputStream);
        Log.i("data","response HTTPClient= "+response);

        return response;
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null){
            result += line;
        }

            /* Close Stream */
        if(null!=inputStream){
            inputStream.close();
        }
        return result;
    }

    String getParseTime(String response){
       String time="";



        return time;
    }
}
